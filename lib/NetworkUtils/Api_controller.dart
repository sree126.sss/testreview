import 'package:dio/dio.dart';
import 'package:flutter_app_test/NetworkUtils/Api_Interceptor.dart';
import 'package:flutter_app_test/models/employee.dart';
import 'package:flutter_app_test/models/employeeDetails.dart';
import 'package:flutter_app_test/models/user.dart';
import 'package:flutter_app_test/models/video.dart';


class ApiController {


  Dio dio = Dio();

  void initDio() {
    dio.options.baseUrl = 'https://api.dailymotion.com/';
    dio.options.connectTimeout = 5000; //5s
    dio.options.receiveTimeout = 3000;
    dio.interceptors.clear();
    dio.interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
    dio.interceptors.add(ApiInterceptor());
  }


  Future<VideoList> getVideoList(int page) async {
    final response = await dio.get('videos?fields=id,title,url,channel,owner,likes_total,views_total,thumbnail_url,url&page=$page');
    return VideoList.fromJson(response.data);
  }

}