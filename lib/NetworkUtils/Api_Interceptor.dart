
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ApiInterceptor extends Interceptor {
 @override
 Future onRequest(RequestOptions options) async {
  return options;
 }

 @override
 Future onError(DioError err) async {

  if (err.response?.statusCode == 401) {
   return Fluttertoast.showToast(msg: 'Invalid Credential');
  } else if (err.response.statusCode == 400) {
   return Fluttertoast.showToast(
    msg: err.response.data['error']['msg'].toString(),
   );
  } else if (err.response.statusCode >= 300 &&
      err.response.statusCode < 500) {
   return Fluttertoast.showToast(
    msg: err.response?.data['error']['msg'] ?? err.message,
   );
  } else if (err.response.statusCode >= 500) {
   await Fluttertoast.showToast(
    msg: 'Server Error. Please Contact the administrator',
   );
  }
 }

 @override
 Future onResponse(Response response) async {}
}