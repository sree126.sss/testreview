import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_app_test/models/video.dart';
import 'package:webview_flutter/webview_flutter.dart';

// ignore: must_be_immutable
class DetailPage extends StatefulWidget {
 videodata data;

 DetailPage({Key key,this.data}):super(key: key);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height ,
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height*.4,
                child: WebView(
                  initialUrl: Uri.dataFromString('<html><body><iframe style="width:100%"   height="300"   src="https://www.dailymotion.com/embed/video/${widget.data.id}"  </iframe></body></html>', mimeType: 'text/html').toString(),
                  javascriptMode: JavascriptMode.unrestricted,
                )),
            Text(widget.data.title??"",style: Theme.of(context).textTheme.headline1),
            SizedBox(height:10),
            Text("${widget.data.channel??" "}",style: Theme.of(context).textTheme.headline2),
            SizedBox(height:10),
            Container(
              width: MediaQuery.of(context).size.width*.8,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Owner : ${widget.data.owner??" "}",style: Theme.of(context).textTheme.headline2),
                  Spacer(),
                  Text("Views",style: Theme.of(context).textTheme.headline2),
                  Icon(Icons.remove_red_eye,size: 18,),
                  SizedBox(width: 5,),
                  Text(widget.data.viewsTotal.toString()??"",style: Theme.of(context).textTheme.headline2),
                  Spacer(),
                  Icon(Icons.thumb_up),
                  SizedBox(width: 5,),
                  Text(widget.data.likesTotal.toString()??"",style: Theme.of(context).textTheme.headline2),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WebView.platform = SurfaceAndroidWebView();
  }
}
