import 'package:animation_wrappers/Animations/faded_translation_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_test/Screen/Home.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: FadedSlideAnimation(
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.black87,
            image: DecorationImage(
              fit: BoxFit.fitHeight,
              image: AssetImage("assets/w.jpg"),
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 70,
              ),
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    child: RichText(
                      text: TextSpan(
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1
                              .copyWith(
                              letterSpacing: 2,
                              fontWeight: FontWeight.bold),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'DailyMotion',
                                style: TextStyle(color: Colors.white,fontSize: 22,fontWeight: FontWeight.bold)),
                            TextSpan(
                                text: 'Player',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,fontSize: 22,fontWeight: FontWeight.bold)),
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 70,
                  ),
                  StreamBuilder(
                    stream: Stream.periodic(const Duration(seconds: 1)),
                    builder: (context, snapshot) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            DateFormat('hh:mm').format(DateTime.now()),
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(fontSize: 60,color: Colors.white),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Text(
                              DateFormat('a')
                                  .format(DateTime.now())
                                  .toLowerCase(),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(fontSize: 15),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
              Spacer(),
              Container(
                alignment: Alignment.bottomCenter,
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Theme.of(context).primaryColor,
                      Colors.transparent,
                    ],
                  ),
                ),
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                  child: GestureDetector(
                    onTap: () {
                      Get.off(HomePage());

                    },
                    child: Text(
                      "Touch To Explore",
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontSize: 20),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        beginOffset: Offset(0.0, 0.4),
        endOffset: Offset(0, 0),
        slideCurve: Curves.linearToEaseOut,
      ),
    );
  }
}