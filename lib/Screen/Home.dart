import 'package:flutter/material.dart';
import 'package:flutter_app_test/Screen/DetailPage.dart';
import 'package:flutter_app_test/controller/VideoController.dart';
import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  int pages=1;

  VideoController videoController = Get.find();
  ScrollController _scrollController = new ScrollController();
  TextEditingController searchController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hit Factory'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width*.90,height: 50,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black26),
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child:  TextField(
                  controller: searchController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                      contentPadding: EdgeInsets.all(10),
                      hintStyle: TextStyle(color: Colors.black26),
                      hintText: "Search Here...",
                      suffixIcon: Icon(
                        Icons.search,
                        color: Colors.black26,
                      )),

                ),
              ),
              SizedBox(height: 20,),
              Obx(()
              {
                 return Container(
                   height: MediaQuery.of(context).size.height*.8,
                   width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                      controller: _scrollController,
                       itemCount: videoController.videoList.length,
                        itemBuilder:(context,index){
                         var data=videoController.videoList[index];
                         if (index == videoController.videoList.length-1) {
                           return Container(
                               height: 100,
                               width: 100,
                               child: Center(child: CircularProgressIndicator()));
                         }
                         else
                         return InkWell(
                           onTap: (){
                             Get.to(DetailPage(data: data));
                           },
                           child: Card(
                             child: Container(
                               height: 110,
                               width: MediaQuery.of(context).size.width,
                               child: Column(
                                 children: [
                                   SizedBox(width: 30,),
                                   Row(
                                     children: [
                                       Image.network(data.thumbnailUrl??"",height:100,width: 100,),
                                       SizedBox(width: 10,),
                                       Row(
                                         mainAxisAlignment: MainAxisAlignment.start,
                                         children: [
                                           Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                             children: [
                                               SizedBox(
                                                   width:MediaQuery.of(context).size.width*.65,
                                                   child: Text(data.title?? "",textAlign: TextAlign.justify,overflow: TextOverflow.visible,maxLines: 2,style: Theme.of(context).textTheme.bodyText1,)),
                                               SizedBox(height: 10,),
                                               Container(
                                                 width:MediaQuery.of(context).size.width*.50,
                                                 child: Row(
                                                   children: [
                                                     Visibility(
                                                       visible: data.channel!=null,
                                                         child: Text(data.channel.toString()??"",style: Theme.of(context).textTheme.bodyText2,)),
                                                     Spacer(),
                                                     Row(
                                                       children: [
                                                         Text("Views",style: Theme.of(context).textTheme.bodyText2),
                                                         Icon(Icons.remove_red_eye,size: 12,color: Colors.teal),
                                                         SizedBox(width: 10,),
                                                         Text(data.viewsTotal.toString()??"",style: Theme.of(context).textTheme.bodyText2),
                                                       ],
                                                     ),
                                                   ],
                                                 ),
                                               ),
                                             ],
                                           ),
                                         ],
                                       ),
                                     ],
                                   ),
                                 ],
                               ),
                             ),
                           ),
                         );
                    } ),
                  );
                }
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    videoController.getVideos(pages);
    searchController.addListener(() {
      videoController.filter(searchController.text);
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
         pages++;
        videoController.getVideos(pages);
      }
    });

  }
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
