import 'package:flutter_app_test/NetworkUtils/Api_controller.dart' ;
import 'package:flutter_app_test/models/video.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class VideoController extends GetxController{
  ApiController apiController =Get.find();
  RxList<videodata>videoList=RxList();
  RxList<videodata>filterData=RxList();


  Future getVideos(int page)async{
    try{
      EasyLoading.show();
      await apiController.getVideoList(page).then((value)async {
        EasyLoading.dismiss();
        videoList.addAll(value.list);
        filterData.addAll(value.list);
      });

    }catch(e){
      EasyLoading.dismiss();
      print(e);
    }

  }
  filter(String value)
  {
    if(value.isNotEmpty)
    {
      var video = filterData.where((element) {
       return element.title.toLowerCase().contains(value.toLowerCase());
        }
      ).toList();
      videoList.clear();
      videoList.addAll(video);
    }
    else{
      videoList.clear();
      videoList.addAll(filterData);
    }
  }

}