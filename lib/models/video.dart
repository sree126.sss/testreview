// To parse this JSON data, do
//
//     final videoList = videoListFromJson(jsonString);

import 'dart:convert';

VideoList videoListFromJson(String str) => VideoList.fromJson(json.decode(str));

String videoListToJson(VideoList data) => json.encode(data.toJson());

class VideoList {
  VideoList({
    this.page,
    this.limit,
    this.explicit,
    this.total,
    this.hasMore,
    this.list,
  });

  int page;
  int limit;
  bool explicit;
  int total;
  bool hasMore;
  List<videodata> list;

  factory VideoList.fromJson(Map<String, dynamic> json) => VideoList(
    page: json["page"] == null ? null : json["page"],
    limit: json["limit"] == null ? null : json["limit"],
    explicit: json["explicit"] == null ? null : json["explicit"],
    total: json["total"] == null ? null : json["total"],
    hasMore: json["has_more"] == null ? null : json["has_more"],
    list: json["list"] == null ? null : List<videodata>.from(json["list"].map((x) => videodata.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "page": page == null ? null : page,
    "limit": limit == null ? null : limit,
    "explicit": explicit == null ? null : explicit,
    "total": total == null ? null : total,
    "has_more": hasMore == null ? null : hasMore,
    "list": list == null ? null : List<dynamic>.from(list.map((x) => x.toJson())),
  };
}

class videodata {
  videodata({
    this.id,
    this.title,
    this.url,
    this.channel,
    this.owner,
    this.likesTotal,
    this.viewsTotal,
    this.thumbnailUrl,

  });

  String id;
  String title;
  String url;
  Channel channel;
  String owner;
  int likesTotal;
  int viewsTotal;
  String thumbnailUrl;

  factory videodata.fromJson(Map<String, dynamic> json) => videodata(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    url: json["url"] == null ? null : json["url"],
    channel: json["channel"] == null ? null : channelValues.map[json["channel"]],
    owner: json["owner"] == null ? null : json["owner"],
    likesTotal: json["likes_total"] == null ? null : json["likes_total"],
    viewsTotal: json["views_total"] == null ? null : json["views_total"],
    thumbnailUrl: json["thumbnail_url"] == null ? null : json["thumbnail_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "url": url == null ? null : url,
    "channel": channel == null ? null : channelValues.reverse[channel],
    "owner": owner == null ? null : owner,
    "likes_total": likesTotal == null ? null : likesTotal,
    "views_total": viewsTotal == null ? null : viewsTotal,
    "thumbnail_url": thumbnailUrl == null ? null : thumbnailUrl,
  };
}

enum Channel { FUN, NEWS, LIFESTYLE }

final channelValues = EnumValues({
  "fun": Channel.FUN,
  "lifestyle": Channel.LIFESTYLE,
  "news": Channel.NEWS
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
