// To parse this JSON data, do
//
//     final employee = employeeFromJson(jsonString);

import 'dart:convert';

Employee employeeFromJson(String str) => Employee.fromJson(json.decode(str));

String employeeToJson(Employee data) => json.encode(data.toJson());

class Employee {
  Employee({
    this.status,
    this.data,
  });

  bool status;
  List<EmployeeData> data;

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<EmployeeData>.from(json["data"].map((x) => EmployeeData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class EmployeeData {
  EmployeeData({
    this.id,
    this.categoryId,
    this.subcategoryId,
    this.userId,
    this.name,
    this.description,
    this.salePrice,
    this.photo,
    this.color,
    this.vatId,
    this.code,
    this.askprice,
    this.askqty,
    this.kitchennote,
    this.weight,
    this.discountable,
    this.cost,
    this.isDelete,
    this.createdAt,
    this.updatedAt,
    this.isInventory,
    this.position,
    this.isOnline,
    this.isPos,
    this.tags,
    this.banner,
    this.productpriceTags,
  });

  int id;
  int categoryId;
  int subcategoryId;
  int userId;
  String name;
  String description;
  int salePrice;
  String photo;
  Color color;
  String vatId;
  String code;
  int askprice;
  int askqty;
  int kitchennote;
  int weight;
  int discountable;
  int cost;
  int isDelete;
  DateTime createdAt;
  DateTime updatedAt;
  int isInventory;
  dynamic position;
  int isOnline;
  int isPos;
  dynamic tags;
  String banner;
  List<ProductpriceTag> productpriceTags;

  factory EmployeeData.fromJson(Map<String, dynamic> json) => EmployeeData(
    id: json["id"] == null ? null : json["id"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    subcategoryId: json["subcategory_id"] == null ? null : json["subcategory_id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
    salePrice: json["sale_price"] == null ? null : json["sale_price"],
    photo: json["photo"] == null ? null : json["photo"],
    color: json["color"] == null ? null : colorValues.map[json["color"]],
    vatId: json["vat_id"] == null ? null : json["vat_id"],
    code: json["code"] == null ? null : json["code"],
    askprice: json["askprice"] == null ? null : json["askprice"],
    askqty: json["askqty"] == null ? null : json["askqty"],
    kitchennote: json["kitchennote"] == null ? null : json["kitchennote"],
    weight: json["weight"] == null ? null : json["weight"],
    discountable: json["discountable"] == null ? null : json["discountable"],
    cost: json["cost"] == null ? null : json["cost"],
    isDelete: json["is_delete"] == null ? null : json["is_delete"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    isInventory: json["is_inventory"] == null ? null : json["is_inventory"],
    position: json["position"],
    isOnline: json["is_online"] == null ? null : json["is_online"],
    isPos: json["is_pos"] == null ? null : json["is_pos"],
    tags: json["tags"],
    banner: json["banner"] == null ? null : json["banner"],
    productpriceTags: json["productpriceTags"] == null ? null : List<ProductpriceTag>.from(json["productpriceTags"].map((x) => ProductpriceTag.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "category_id": categoryId == null ? null : categoryId,
    "subcategory_id": subcategoryId == null ? null : subcategoryId,
    "user_id": userId == null ? null : userId,
    "name": name == null ? null : name,
    "description": description == null ? null : description,
    "sale_price": salePrice == null ? null : salePrice,
    "photo": photo == null ? null : photo,
    "color": color == null ? null : colorValues.reverse[color],
    "vat_id": vatId == null ? null : vatId,
    "code": code == null ? null : code,
    "askprice": askprice == null ? null : askprice,
    "askqty": askqty == null ? null : askqty,
    "kitchennote": kitchennote == null ? null : kitchennote,
    "weight": weight == null ? null : weight,
    "discountable": discountable == null ? null : discountable,
    "cost": cost == null ? null : cost,
    "is_delete": isDelete == null ? null : isDelete,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "is_inventory": isInventory == null ? null : isInventory,
    "position": position,
    "is_online": isOnline == null ? null : isOnline,
    "is_pos": isPos == null ? null : isPos,
    "tags": tags,
    "banner": banner == null ? null : banner,
    "productpriceTags": productpriceTags == null ? null : List<dynamic>.from(productpriceTags.map((x) => x.toJson())),
  };
}

enum Color { THE_303030, FF8080 }

final colorValues = EnumValues({
  "#ff8080": Color.FF8080,
  "#303030": Color.THE_303030
});

class ProductpriceTag {
  ProductpriceTag({
    this.productId,
    this.pricetagsId,
    this.price,
  });

  int productId;
  int pricetagsId;
  int price;

  factory ProductpriceTag.fromJson(Map<String, dynamic> json) => ProductpriceTag(
    productId: json["product_id"] == null ? null : json["product_id"],
    pricetagsId: json["pricetags_id"] == null ? null : json["pricetags_id"],
    price: json["price"] == null ? null : json["price"],
  );

  Map<String, dynamic> toJson() => {
    "product_id": productId == null ? null : productId,
    "pricetags_id": pricetagsId == null ? null : pricetagsId,
    "price": price == null ? null : price,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
