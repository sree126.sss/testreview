// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.userId,
    this.user,
    this.token,
    this.status,
  });

  int userId;
  UserClass user;
  String token;
  bool status;

  factory User.fromJson(Map<String, dynamic> json) => User(
    userId: json["user_id"] == null ? null : json["user_id"],
    user: json["user"] == null ? null : UserClass.fromJson(json["user"]),
    token: json["token"] == null ? null : json["token"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId == null ? null : userId,
    "user": user == null ? null : user.toJson(),
    "token": token == null ? null : token,
    "status": status == null ? null : status,
  };
}

class UserClass {
  UserClass({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.mobile,
    this.address,
    this.role,
    this.createdAt,
    this.updatedAt,
    this.enabled,
    this.onlineKey,
    this.mainTerminal,
    this.enableTableSideOrdering,
  });

  int id;
  String name;
  String email;
  dynamic emailVerifiedAt;
  String mobile;
  String address;
  String role;
  DateTime createdAt;
  DateTime updatedAt;
  int enabled;
  String onlineKey;
  int mainTerminal;
  int enableTableSideOrdering;

  factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    emailVerifiedAt: json["email_verified_at"],
    mobile: json["mobile"] == null ? null : json["mobile"],
    address: json["address"] == null ? null : json["address"],
    role: json["role"] == null ? null : json["role"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    enabled: json["enabled"] == null ? null : json["enabled"],
    onlineKey: json["online_key"] == null ? null : json["online_key"],
    mainTerminal: json["main_terminal"] == null ? null : json["main_terminal"],
    enableTableSideOrdering: json["enable_table_side_ordering"] == null ? null : json["enable_table_side_ordering"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "email_verified_at": emailVerifiedAt,
    "mobile": mobile == null ? null : mobile,
    "address": address == null ? null : address,
    "role": role == null ? null : role,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "enabled": enabled == null ? null : enabled,
    "online_key": onlineKey == null ? null : onlineKey,
    "main_terminal": mainTerminal == null ? null : mainTerminal,
    "enable_table_side_ordering": enableTableSideOrdering == null ? null : enableTableSideOrdering,
  };
}
