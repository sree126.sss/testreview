// To parse this JSON data, do
//
//     final employeeDetails = employeeDetailsFromJson(jsonString);

import 'dart:convert';

List<EmployeeDetails> employeeDetailsFromJson(String str) => List<EmployeeDetails>.from(json.decode(str).map((x) => EmployeeDetails.fromJson(x)));

String employeeDetailsToJson(List<EmployeeDetails> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmployeeDetails {
  EmployeeDetails({
    this.id,
    this.imageUrl,
    this.firstName,
    this.lastName,
    this.email,
    this.contactNumber,
    this.age,
    this.dob,
    this.salary,
    this.address,
  });

  int id;
  String imageUrl;
  String firstName;
  String lastName;
  String email;
  String contactNumber;
  int age;
  String dob;
  int salary;
  String address;

  factory EmployeeDetails.fromJson(Map<String, dynamic> json) => EmployeeDetails(
    id: json["id"] == null ? null : json["id"],
    imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    email: json["email"] == null ? null : json["email"],
    contactNumber: json["contactNumber"] == null ? null : json["contactNumber"],
    age: json["age"] == null ? null : json["age"],
    dob: json["dob"] == null ? null : json["dob"],
    salary: json["salary"] == null ? null : json["salary"],
    address: json["address"] == null ? null : json["address"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "imageUrl": imageUrl == null ? null : imageUrl,
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "email": email == null ? null : email,
    "contactNumber": contactNumber == null ? null : contactNumber,
    "age": age == null ? null : age,
    "dob": dob == null ? null : dob,
    "salary": salary == null ? null : salary,
    "address": address == null ? null : address,
  };
}
