import 'package:flutter/material.dart';
import 'package:flutter_app_test/NetworkUtils/Api_controller.dart';
import 'package:flutter_app_test/Screen/Home.dart';
import 'package:flutter_app_test/controller/VideoController.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:flutter_app_test/Screen/LandingPage.dart';

void main() async{
  Get.put(ApiController(), permanent: true);
  Get.put(VideoController(), permanent: true);
  Get.find<ApiController>().initDio();
  await GetStorage.init();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        accentColor: Colors.indigo,
        textTheme: TextTheme(
          headline1: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),
          bodyText1: TextStyle(color: Colors.black87,fontSize: 14),
          headline2: TextStyle(color: Colors.black,fontSize: 14),
          bodyText2:  TextStyle(color: Colors.black,fontSize: 12),
        )
      ),
      builder: EasyLoading.init(),
      home: LandingPage(),
    );
  }
}

